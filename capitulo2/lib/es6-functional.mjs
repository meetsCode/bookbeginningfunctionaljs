



/*
//Este es la versión original pero no
// funciona por el export no va
const forEach = (array,fn) => {
  let i;
  for (i=0;i<array.length;i++)
    fn(array[i])
}

export default forEach
*/

// Esta la versión está modificiada
// por lo leído en:
// https://nodejs.org/dist/latest-v12.x/docs/api/esm.html#esm_ecmascript_modules
// funciona por el export no va
const forEach = (array,fn) => {
  let i;
  for (i=0;i<array.length;i++)
    fn(array[i]);
}

const some = (array, fn) => {
  let result = true;
  for(const value of array)
    result = result || fn(value)
  return result
}

const every = (array, fn) => {
  let result = true;
  for(const value of array)
    result = result && fn(value)
  return result
}

export { forEach , some , every };
