/* import forEach from "../lib/es6-functional.js" ;
var array[1,2,3]
forEach[array, (data) => console.log(data)] //refereing to imported forEach
*/


//import foreachy from "../lib/es6-functional.js" ;
/*
const cada = require('../lib/es6-functional.js');


console.log("data") ;

const forEach = (array, fn) => {
  let i;
  for(i=0; i<array.length; i++)
    fn(array[i])
} ;
var array=[1,2,3] ;
forEach(array, (data) => console.log(data) ); //refereing to imported forEach
cada.foreachy(array, (data) => console.log(data) ); //refereing to imported forEach
*/





/*
//Este es la versión original pero no
// funciona por el import no va
import forEach from '../lib/es6-functional.mjs'


var array = [1,2, 3]


forEach (array, (data) => {
  console.log(data)
} )
*/



//const circle = require('../lib/es6-functional.mjs');

// ejecuto con el comando:
// $ node functional-playground/play.mjs

import { forEach , every, some } from '../lib/es6-functional.mjs'


var array = [1,2, 3]


forEach (array, (data) => {
  console.log(data)
} );

console.log( every([NaN, NaN, NaN], isNaN) );

console.log( every([NaN, NaN, NaN], isNaN) );
const fn2 = (a,b) => (a+b, a*b);
const fn3 = (a,b,c) => (a+b, a*b*c);
const fn4 = (a,b,c,d) => (a+b, a*b*d);
console.log( fn2(1,2) );
console.log( fn3(1,2,3) );
console.log( fn4(1,2,3,4) );

console.log( fn2.length );
console.log( fn3.length );
console.log( fn4.length );

[1,2,3].map( (a,b,c) => console.log(a,b ,c) );

const fnn = (a, b, c) => (console.log('el2'), console.log('el2') , console.log(3) , 4*a*b*c);
fnn(1,2,3);
console.log( fnn(1,2,3));
