/* import forEach from "../lib/es6-functional.js" ;
var array[1,2,3]
forEach[array, (data) => console.log(data)] //refereing to imported forEach
*/


//import foreachy from "../lib/es6-functional.js" ;
/*
const cada = require('../lib/es6-functional.js');


console.log("data") ;

const forEach = (array, fn) => {
  let i;
  for(i=0; i<array.length; i++)
    fn(array[i])
} ;
var array=[1,2,3] ;
forEach(array, (data) => console.log(data) ); //refereing to imported forEach
cada.foreachy(array, (data) => console.log(data) ); //refereing to imported forEach
*/





/*
//Este es la versión original pero no
// funciona por el import no va
import forEach from '../lib/es6-functional.mjs'


var array = [1,2, 3]


forEach (array, (data) => {
  console.log(data)
} )
*/



//const circle = require('../lib/es6-functional.mjs');



import {forEach} from '../lib/es6-functional.mjs'


var array = [1,2, 3]


forEach (array, (data) => {
  console.log(data)
} )
